console.log("Hello World"); 


//Assignment Operator - assign value

let assignmentNumber = 8;

//Arithmetic Operators
// + - * / %

//Addition Assignment Operator(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber);


//subtraction/multiplication/division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

let z = 1;

++z;
console.log(z); 

z++;
console.log(z); 
console.log(z++); 
console.log(z); 

console.log(z++);
console.log(z); 

console.log(++z); 

console.log(--z); 
console.log(z--); 
console.log(z); 

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion)
console.log(typeof nonCoercion)

let numE = true + 1;
console.log(numE); 
console.log(typeof numE); 

let numF = false + 1;
console.log(numF);

let juan = 'juan';

console.log(1 == 1); 
console.log(1 == 2); 
console.log(1 == '1'); 
console.log(0 == false); 
console.log('juan' == 'JUAN'); 
console.log('juan' == juan); 

console.log(1 != 1); 
console.log(1 != 2);
console.log(1 != '1'); 
console.log(0 != false);
console.log('juan' != 'JUAN');
console.log('juan' != juan);

console.log('Strictly Equality Operator')
console.log(1 === 1); 
console.log(1 === 2); 
console.log(1 === '1'); 
console.log(0 === false); 
console.log('juan' === 'JUAN'); 
console.log('juan' === juan); 

console.log('Strictly Inequality Operator')
console.log(1 !== 1); 
console.log(1 !== 2); 
console.log(1 !== '1'); 
console.log(0 !== false); 
console.log('juan' !== 'JUAN');
console.log('juan' !== juan);

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("Greater than")
console.log(x > y);
console.log(w > y); 

console.log("Less than")
console.log(w < x); 
console.log(y < y); 
console.log(x < 1000); 
console.log(numString < 1000); 
console.log(numString < 6000); 
console.log(numString < "Jose"); 

console.log(w >= 8000); 

console.log(x <= y); 
console.log(y <= y); 

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical AND Operators")
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); 
let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); 

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); 

let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1); 

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); 

console.log("Logical OR Operator");

let userLevel = 100;
let userLevel2 = 65;


let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); 

let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2); 

console.log("Not Operator")

let guildAdmin = !isAdmin || userLevel2 >=requiredLevel;
console.log(guildAdmin);

console.log(!isRegistered); 
console.log(!isLegalAge); 

let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite); 
console.log(opposite2);

let numG = -1;

if(numG < 0) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Welcome to Game Online!");
}
if(userLevel3 >= requiredLevel) {
	console.log("You are qualified to join the guild!")
}

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("Thank you for joining the Admin!")
} else {
	console.log("You are not ready to be an Admin")
}


if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noobies Guild!");
} else {
	console.log("You are too strong to be a noob. :( ")
}



if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the noobies guild.")
} else if(userLevel > 25) {
	console.log("You too strong to be a noob.")
} else if(userAge3 < requiredAge) {
	console.log("You're too young to join the guild.")
} else {
	console.log("better luck next time.")
}


function addNum(num1, num2) {

	if(typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers");
	}
}

addNum(5, '2');

let numsample = true;
console.log(typeof numsample)


function login(username, password)
{

	if(typeof username === "string" && typeof password === "string")
		{console.log("Both Arguments are strings.")
				if(username.length >= 8 && password.length >= 8)
					{console.log("Thank you for logging in.")} 
				else if(username.length <= 8)	
					{console.log("Username is too short.")} 
				else if(password.length <= 8)
					{console.log("Password is too short.")}
				else (username.length <= 8 || password.length <= 8)
					{console.log("Credentials too short.")}
		}	
	else 
		{console.log("One of the arguments is not a string")}
}


login("janes", "jane12")


// else if ternary operator

let a = 7;
7===5
? console.log("A")
:(a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));


// multiple statement excution

// let name;

// function isOfLegalAge(){
// 	name = 'John';
// 	return 'you are of the legal age limit';
// }

// function isOfUnderAge(){
// 	name = 'John';
// 	return 'you are under the age limit';
// }

// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age>18) ? isOfLegalAge() : isOfUnderAge();
// console.log(`Result of ternary operator in functions: ${legalAge}, ${name}`);


// mini activity

function color(day){
	if(typeof day === "string"){
		if(day === "Monday"){
			alert(`Today is ${day}. Wear Black.`)
		}
		else{
			alert("Invalid input")
		}
	} else {
		alert('Invalid input. Enter a valid day of the week.')
	}
}

let day = prompt("What day is today?")
color(day)


//switch - if alam yung input user if not if else else if ang gagamitin

let hero = prompt("Type a herp").toLowerCase();